# A "magical" Makefile
# Quick guide:
# 1. Import this file as a subtree (e.g. /Makefiles)
# 2. Add a Makefile at the project root (e.g. /Makefile)
# 3. Define inside that Makefile variables (e.g. EXECS, sources for each EXEC, etc.)
# 4. include this makefile from your root makefile (e.g. "include Makefiles/Makefile")
# 5. Profit

.DEFAULT_GOAL=all
.PHONY: all clean

# EXECS and LIBS should be previously defined (or left empty) to
# detail what you want to build
# See the example file for some ideas
# LIBS is currently not implemented
all: $(EXECS) $(LIBS)

clean:
	rm -rf build

%/:
	mkdir --parent "$@"

# You can define those to force CFLAGS and LDFLAGS
LDFLAGS ?=
CFLAGS ?=

# You can change where build place are placed
BUILD_PATH ?= build

# Those are the LDFLAGS and CFLAGS we pass by default If you really
# must, these are passed first to so you should be able to override
# them easily
DEFAULTLDFLAGS :=
DEFAULTCFLAGS := -Wall -Wextra -pedantic -std=c17

# >>>>>
# Here the magic starts
# You don't need to read anything below this

# Someone needs wayland, let's import it
ifneq ($(strip $(foreach x,$(EXECS) $(LIBS),$(value $(x)_WAYLAND_CLIENT_PROTOCOLS) $(value $(x)_WAYLAND_SERVER_PROTOCOLS))),)
include Makefile.d/wayland.ml
endif

define CC_OBJ_TEMPLATE =
$(1): $(2) | $(3)
	$(CC) $(4) -c $(2) -o $(1)
endef

define CC_OBJ_INPLACE_TEMPLATE =
$(call CC_OBJ_TEMPLATE,$(patsubst %.c,%.o,$(1)),$(1),$(dir $(1)),$(2))
endef

define CC_OBJ_INBUILD_TEMPLATE =
$(call CC_OBJ_TEMPLATE,build/$(1)/$(patsubst %.c,%.o,$(2)),$(2),build/$(1)/$(dir $(2)),$(3))
endef

define LDFLAGS_TEMPLATE =
$(1)_LDFLAGS := $(DEFAULTLDFLAGS) $(shell pkg-config --libs $(value $(1)_LIBS)) $(LDFLAGS)
endef

define CFLAGS_TEMPLATE =
$(1)_CFLAGS := $(DEFAULTCFLAGS) -I./include $(shell pkg-config --cflags $(value $(1)_LIBS)) $(CFLAGS)
endef

define SRCS_OBJS_TEMPLATE =
$(foreach src,$($(1)_SRCS), $(eval $(call CC_OBJ_INBUILD_TEMPLATE,$(1),$(src),$($(1)_CFLAGS))))
$(1)_SRCS_OBJS := $(foreach src,$($(1)_SRCS),build/$(1)/$(patsubst %.c,%.o,$(src)))
endef

define OBJS_TEMPLATE =
$(1)_OBJS := $($(1)_WAYLAND_PROTO_OBJS) $($(1)_SRCS_OBJS)
endef

define EXEC_TEMPLATE =
$(eval $(call LDFLAGS_TEMPLATE,$(1)))
$(eval $(call CFLAGS_TEMPLATE,$(1)))
$(eval $(call SRCS_OBJS_TEMPLATE,$(1)))
$(eval $(call USES_WAYLAND_TEMPLATE,$(1)))
ifeq ($($(1)_USES_WAYLAND_PROTO),y)
$(eval $(call WAYLAND_PROTO_TEMPLATE,$(1)))
endif
$(eval $(call OBJS_TEMPLATE,$(1)))
$(1): $($(1)_OBJS)
	$(CC) $($(1)_LDFLAGS) $($(1)_OBJS) -o $(1)
endef

$(foreach EXEC,$(EXECS),$(eval $(call EXEC_TEMPLATE,$(EXEC))))

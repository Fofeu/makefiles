WAYLAND_SCANNER := $(shell pkg-config --variable=wayland_scanner wayland-scanner)

define WAYLAND_PROTO_GEN_CLT_HDR_TEMPLATE =
$(1): $(2) | $(dir $(1))
	$(WAYLAND_SCANNER) client-header $(2) $(1)
endef

define WAYLAND_PROTO_GEN_SRV_HDR_TEMPLATE =
$(1): $(2) | $(dir $(1))
	$(WAYLAND_SCANNER) server-header $(2) $(1)
endef

define WAYLAND_PROTO_GEN_CODE_TEMPLATE =
$(1): $(2) | $(dir $(1))
	$(WAYLAND_SCANNER) private-code $(2) $(1)
endef

# Template to define utilities
define WAYLAND_PROTO_STAGE_1_TEMPLATE =
$(1)_CFLAGS += -I./build/$(1)/protocols
$(1)_WAYLAND_PROTO_XML = $(addsuffix .xml,$(addprefix protocols/,$$(1)))
$(1)_WAYLAND_PROTO_CLT_INC = $(addsuffix -client-protocol.h,$(addprefix build/$(1)/protocols/,$$(1)))
$(1)_WAYLAND_PROTO_SRV_INC = $(addsuffix -server-protocol.h,$(addprefix build/$(1)/protocols/,$$(1)))
$(1)_WAYLAND_PROTO_SRC = $(addsuffix -protocol.c,$(addprefix build/$(1)/protocols/,$$(1)))
$(1)_WAYLAND_PROTO_OBJ = $(addsuffix -protocol.o,$(addprefix build/$(1)/protocols/,$$(1)))
endef

# Template to define the collection of files we care about
define WAYLAND_PROTO_STAGE_2_TEMPLATE =
$(1)_WAYLAND_PROTO_CLT_INCS = $(foreach proto,$($(1)_WAYLAND_CLIENT_PROTOCOLS), $(call $(1)_WAYLAND_PROTO_CLT_INC,$(proto)))
$(1)_WAYLAND_PROTO_SRV_INCS = $(foreach proto,$($(1)_WAYLAND_SERVER_PROTOCOLS), $(call $(1)_WAYLAND_PROTO_SRV_INC,$(proto)))
$(1)_WAYLAND_PROTO_SRCS = $(foreach proto,$(sort $($(1)_WAYLAND_CLIENT_PROTOCOLS) $($(1)_WAYLAND_SERVER_PROTOCOLS)), $(call $(1)_WAYLAND_PROTO_SRC,$(proto)))
$(1)_WAYLAND_PROTO_OBJS = $(foreach proto,$(sort $($(1)_WAYLAND_CLIENT_PROTOCOLS) $($(1)_WAYLAND_SERVER_PROTOCOLS)), $(call $(1)_WAYLAND_PROTO_OBJ,$(proto)))
$(1)_WAYLAND_PROTOCOLS = $(sort $($(1)_WAYLAND_CLIENT_PROTOCOLS) $($(1)_WAYLAND_SERVER_PROTOCOLS))
endef

# Template to define the actual compilation process
define WAYLAND_PROTO_STAGE_3_TEMPLATE =
$(foreach src,$($(1)_WAYLAND_PROTO_SRCS), $(eval $(call CC_OBJ_INPLACE_TEMPLATE,$(src),$($(1)_CFLAGS))))
$(foreach proto,$($(1)_WAYLAND_CLT_PROTOCOLS), $(eval $(call WAYLAND_PROTO_GEN_CLT_HDR_TEMPLATE,$(call $(1)_WAYLAND_PROTO_CLT_INC,$(proto)),$(call $(1)_WAYLAND_PROTO_XML,$(proto)))))
$(foreach proto,$($(1)_WAYLAND_SRV_PROTOCOLS), $(eval $(call WAYLAND_PROTO_GEN_SRV_HDR_TEMPLATE,$(call $(1)_WAYLAND_PROTO_SRV_INC,$(proto)),$(call $(1)_WAYLAND_PROTO_XML,$(proto)))))
$(foreach proto,$($(1)_WAYLAND_PROTOCOLS), $(eval $(call WAYLAND_PROTO_GEN_CODE_TEMPLATE,$(call $(1)_WAYLAND_PROTO_SRC,$(proto)),$(call $(1)_WAYLAND_PROTO_XML,$(proto)))))
endef

define WAYLAND_PROTO_TEMPLATE =
$(eval $(call WAYLAND_PROTO_STAGE_1_TEMPLATE,$(1)))
$(eval $(call WAYLAND_PROTO_STAGE_2_TEMPLATE,$(1)))
$(eval $(call WAYLAND_PROTO_STAGE_3_TEMPLATE,$(1)))
endef


define USES_WAYLAND_TEMPLATE =
ifneq ($(strip $($(1)_WAYLAND_CLIENT_PROTOCOLS) $($(1)_WAYLAND_SERVER_PROTOCOLS)),)
$(1)_USES_WAYLAND_PROTO := y
endif
endef
